# vagrant_lamp_traversy
Simple LAMP stack Vagrant box  
[Vagrant Crash Course - 46:30](https://youtu.be/vBreXjkizgo "In this crash course we will dive into Vagrant which is used to create isolated & portable development environments. We will go into each part of the Vagrantfile, do some exploring via SSH and create a LAMP stack box (Linux, Apache, MySQL, PHP).")
